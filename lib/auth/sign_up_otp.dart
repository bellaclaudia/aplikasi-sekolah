// ignore_for_file: avoid_print

import 'dart:async';
import 'package:aplikasi_sekolah/address/lengkapi_alamat.dart';
import 'package:aplikasi_sekolah/auth/sign_up.dart';
import 'package:aplikasi_sekolah/templates/auth/sign_up_otp.dart';
import 'package:aplikasi_sekolah/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:get_it/get_it.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:supabase/supabase.dart';

class SignUpOtp extends StatefulWidget {
  const SignUpOtp({Key key, this.verificationId, this.nohandphone})
      : super(key: key);
  final String verificationId;
  final String nohandphone;
  @override
  SignUpOtpState createState() => SignUpOtpState();
}

class SignUpOtpState extends State<SignUpOtp>
    with SingleTickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  final TextEditingController pinPutController = TextEditingController();
  final FocusNode pinPutFocusNode = FocusNode();

  bool errorLogin = false;
  String errorLoginText = '';

  AnimationController animationController;

  SharedPreferences sharedPreferences;
  String tokenFirebase;

  @override
  void initState() {
    animationController = AnimationController(
        duration: const Duration(milliseconds: 600), vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    pinPutFocusNode.dispose();
    super.dispose();
  }

  Future<bool> _onBackPressed() async {
    Navigator.pushReplacement(
        context,
        PageRouteBuilder(
            pageBuilder: (c, a1, a2) => const SignUp(),
            transitionsBuilder: (c, anim, a2, child) => SlideTransition(
                  position: Tween<Offset>(
                    begin: const Offset(1, 0),
                    end: Offset.zero,
                  ).animate(anim),
                  child: child,
                ),
            transitionDuration: const Duration(milliseconds: 500)));
  }

  void onSignUpPressed() {
    phoneNumberVerification();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onBackPressed,
      child: SignUpOtpTemplate(
          title: "Kode OTP Terkirim",
          description:
              "Masukkan kode OTP yang terkirim via SMS ke nomor handphone yang Anda daftarkan " +
                  widget.nohandphone,
          bgImage: const AssetImage("assets/bg.png"),
          logoImage: const AssetImage("assets/logo.png"),
          scaffoldKey: _scaffoldKey,
          signUpLabel: "Sign Up",
          onSignUpPressed: onSignUpPressed,
          pinPutController: pinPutController,
          pinPutFocusNode: pinPutFocusNode),
    );
  }

  Future<List> phoneNumberVerification() async {
    ProgressDialog pr = ProgressDialog(context,
        type: ProgressDialogType.Download, isDismissible: true, showLogs: true);
    progressDialog(pr, context, 'Mohon tunggu...');

    await pr.show();
    final res = await submitDaftar();
    if (res.error?.message == null) {
      pr.hide();
      final users = await submitSignInWithPhone();
      if (users.error?.message == null) {
        pr.hide();
        final insertUsers =
            await GetIt.instance<SupabaseClient>().from('users').insert([
          {
            "id": users.data.user.id,
            "phone": widget.nohandphone,
            "is_active": true,
            "password": dotenv.env['PASS']
          }
        ]).execute();
        await GetIt.instance<SupabaseClient>().from('model_has_roles').insert([
          {
            "role_id": 1,
            "model_type": "User",
            "model_id": insertUsers.data[0]['id']
          }
        ]).execute();
        await GetIt.instance<SupabaseClient>().from('user_details').insert([
          {"user_id": insertUsers.data[0]['id']}
        ]).execute();

        sharedPreferences = await SharedPreferences.getInstance();
        await sharedPreferences.setString(
            'user', users.data.persistSessionString);
        await sharedPreferences.setString(
            'user_detail', insertUsers.data[0].toString());
        Navigator.pushReplacement(
            context,
            PageRouteBuilder(
                pageBuilder: (c, a1, a2) => const LengkapiAlamat(),
                transitionsBuilder: (c, anim, a2, child) => SlideTransition(
                      position: Tween<Offset>(
                        begin: const Offset(1, 0),
                        end: Offset.zero,
                      ).animate(anim),
                      child: child,
                    ),
                transitionDuration: const Duration(milliseconds: 500)));
      } else {
        pr.hide();
        showErrorSignUpSignIn(context, users.error?.message, "signup");
      }
    } else {
      pr.hide();
      showErrorSignUpSignIn(context, res.error?.message, "signup");
    }
  }

  Future<GotrueSessionResponse> submitDaftar() async {
    final result = await GetIt.instance<SupabaseClient>()
        .auth
        .verifyOTP(widget.nohandphone, pinPutController.text);
    return result;
  }

  Future<GotrueSessionResponse> submitSignInWithPhone() async {
    final result = await GetIt.instance<SupabaseClient>()
        .auth
        .signIn(phone: widget.nohandphone, password: dotenv.env['PASS']);
    return result;
  }
}
