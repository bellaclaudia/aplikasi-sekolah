// ignore_for_file: avoid_print

import 'dart:async';
import 'package:aplikasi_sekolah/auth/sign_up_otp.dart';
import 'package:aplikasi_sekolah/templates/auth/sign_up.dart';
import 'package:aplikasi_sekolah/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:get_it/get_it.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:supabase/supabase.dart';

class SignUp extends StatefulWidget {
  const SignUp({Key key}) : super(key: key);
  @override
  SignUpState createState() => SignUpState();
}

class SignUpState extends State<SignUp> with SingleTickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  final FocusNode myFocusNodePhone = FocusNode();
  TextEditingController phoneController = TextEditingController();

  bool errorLogin = false;
  String errorLoginText = '';

  AnimationController animationController;

  SharedPreferences sharedPreferences;
  String tokenFirebase;

  @override
  void initState() {
    animationController = AnimationController(
        duration: const Duration(milliseconds: 600), vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    myFocusNodePhone.dispose();
    super.dispose();
  }

  Future<bool> _onBackPressed() async {
    Navigator.of(context).pop();
  }

  void onSignUpPressed() async {
    ProgressDialog pr = ProgressDialog(context,
        type: ProgressDialogType.Download, isDismissible: true, showLogs: true);
    progressDialog(pr, context, 'Mohon tunggu...');

    await pr.show();
    final res = await submitSignUpWithPhone();
    if (res.error?.message == null) {
      pr.hide();
      Navigator.pushReplacement(
          context,
          PageRouteBuilder(
              pageBuilder: (c, a1, a2) => SignUpOtp(
                  verificationId: "abc", nohandphone: phoneController.text),
              transitionsBuilder: (c, anim, a2, child) => SlideTransition(
                    position: Tween<Offset>(
                      begin: const Offset(1, 0),
                      end: Offset.zero,
                    ).animate(anim),
                    child: child,
                  ),
              transitionDuration: const Duration(milliseconds: 500)));
    } else {
      pr.hide();
      showErrorSignUpSignIn(context, res.error?.message, "signup");
    }
  }

  Future<GotrueSessionResponse> submitSignUpWithPhone() async {
    final result = await GetIt.instance<SupabaseClient>()
        .auth
        .signUpWithPhone(phoneController.text, dotenv.env['PASS']);
    return result;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onBackPressed,
      child: SignUpTemplate(
        title: "Sign Up",
        subTitle: "Join our team!",
        description:
            "We’re looking for amazing engineers just like you! Become a part of our rockstar engineering team and skyrocket your career!",
        bgImage: const AssetImage("assets/bg.png"),
        logoImage: const AssetImage("assets/logo.png"),
        scaffoldKey: _scaffoldKey,
        signUpLabel: "Sign Up",
        onSignUpPressed: onSignUpPressed,
        myFocusNodePhone: myFocusNodePhone,
        phoneController: phoneController,
      ),
    );
  }
}
