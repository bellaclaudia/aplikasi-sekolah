import 'package:aplikasi_sekolah/auth/sign_in.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:aplikasi_sekolah/utils/delayed_animation.dart';

class Splash extends StatefulWidget {
  const Splash({Key key}) : super(key: key);

  @override
  SplashState createState() => SplashState();
}

class SplashState extends State<Splash> with SingleTickerProviderStateMixin {
  final int delayedAmount = 500;

  AnimationController animationController;
  Animation<double> animation;

  SharedPreferences sharedPreferences;
  String apiToken, tokenFirebase, userNama;
  bool boolLogin;

  @override
  void initState() {
    startTime();
    animationController =
        AnimationController(vsync: this, duration: const Duration(seconds: 2));
    animation =
        CurvedAnimation(parent: animationController, curve: Curves.easeOut);
    animation.addListener(() => setState(() {}));
    animationController.forward();
    super.initState();
  }

  startTime() async {
    sharedPreferences = await SharedPreferences.getInstance();
    boolLogin = sharedPreferences.getBool("login");
    sharedPreferences.commit();

    if (boolLogin != null) {
      if (!boolLogin) {
        var _duration = const Duration(seconds: 4);
        return Timer(_duration, navigationPage);
      } else {
        return Navigator.pushReplacement(
            context,
            PageRouteBuilder(
                pageBuilder: (c, a1, a2) => const SignIn(),
                transitionsBuilder: (c, anim, a2, child) => SlideTransition(
                      position: Tween<Offset>(
                        begin: const Offset(1, 0),
                        end: Offset.zero,
                      ).animate(anim),
                      child: child,
                    ),
                transitionDuration: const Duration(milliseconds: 500)));
      }
    } else {
      var _duration = const Duration(seconds: 4);
      return Timer(_duration, navigationPage);
    }
  }

  void navigationPage() async {
    Navigator.pushReplacement(
        context,
        PageRouteBuilder(
            pageBuilder: (c, a1, a2) => const SignIn(),
            transitionsBuilder: (c, anim, a2, child) => SlideTransition(
                  position: Tween<Offset>(
                    begin: const Offset(1, 0),
                    end: Offset.zero,
                  ).animate(anim),
                  child: child,
                ),
            transitionDuration: const Duration(milliseconds: 500)));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      children: <Widget>[
        Container(
            decoration: const BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/bg.png"), fit: BoxFit.fill))),
        Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              DelayedAnimation(
                child: Image.asset('assets/logo.png',
                    width: 200, height: 200, fit: BoxFit.fill),
                delay: delayedAmount + 1000,
              )
            ],
          ),
        )
      ],
    ));
  }
}
