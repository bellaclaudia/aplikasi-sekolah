import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:aplikasi_sekolah/libraries/animated_dialog_box.dart';

void progressDialog(ProgressDialog pr, BuildContext context, String text) {
  pr.style(
      message: text,
      borderRadius: 10.0,
      backgroundColor: Colors.white,
      progressWidget: const CircularProgressIndicator(),
      elevation: 10.0,
      insetAnimCurve: Curves.easeInOut,
      progressTextStyle: const TextStyle(
          color: Colors.transparent,
          fontSize: 13.0,
          fontWeight: FontWeight.w400),
      messageTextStyle: const TextStyle(
          color: Colors.black, fontSize: 19.0, fontWeight: FontWeight.w600));
}

void showErrorSignUpSignIn(
    BuildContext context, String message, String type) async {
  await animated_dialog_box.showInOutDailog(
      title: const Center(
          child: Text('Error', style: TextStyle(color: Colors.black))),
      context: context,
      firstButton: MaterialButton(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(40),
            side: const BorderSide(color: Colors.black, width: 1)),
        color: Colors.white,
        child: const Text('Tutup', style: TextStyle(color: Colors.black)),
        onPressed: () async {
          Navigator.of(context).pop();
        },
      ),
      icon: const Icon(
        null,
        size: 0,
      ),
      yourWidget: Text(message,
          style: TextStyle(
            fontFamily: 'Aller',
            fontSize: 14,
            color: Colors.black.withOpacity(0.5),
          )));
}
