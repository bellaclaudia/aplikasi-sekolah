import 'package:flutter/material.dart';

class SignInTemplate extends StatefulWidget {
  
  const SignInTemplate({
    Key key,
    this.bgImage,
    this.logoImage,
    this.scaffoldKey,
    this.title,
    this.subTitle,
    this.description,
    this.myFocusNodePhone,
    this.phoneController,
    this.onSignInPressed,
    this.onSignUpPressed,
    this.signInLabel,
    this.signUpLabel
  }) : super(key: key);

  final AssetImage bgImage;
  final AssetImage logoImage;
  final GlobalKey<ScaffoldState> scaffoldKey;
  final String title;
  final String subTitle;
  final String description;
  final FocusNode myFocusNodePhone;
  final TextEditingController phoneController;
  final Function onSignInPressed;
  final Function onSignUpPressed;
  final String signInLabel;
  final String signUpLabel;

  @override
  State<SignInTemplate> createState() => _SignInTemplateState();
}

class _SignInTemplateState extends State<SignInTemplate> with SingleTickerProviderStateMixin {
  
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        image: DecorationImage(
          image: widget.bgImage,
          fit: BoxFit.cover
        )
      ),
      padding: const EdgeInsets.only(top: 24, left: 16, right: 16),
      child: Scaffold(
        key: widget.scaffoldKey,
        backgroundColor: Colors.transparent,
        body: LayoutBuilder(
          builder: (context, viewportConstraints) {
            return SingleChildScrollView(
              child: ConstrainedBox(
                constraints: viewportConstraints.copyWith(
                  minHeight: viewportConstraints.maxHeight,
                  maxHeight: double.infinity,
                ),
                child: Flex(
                  direction: Axis.vertical,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      widget.title,
                      style: const TextStyle(
                        fontFamily: 'sans-serif',
                        fontSize: 30,
                        color: Color(0xFF1A202C),
                        fontWeight: FontWeight.bold
                      ),
                      textAlign: TextAlign.left,
                    ),
                    const SizedBox(
                      height: 24,
                    ),
                    Center(
                      child: Container(
                        width: 120.0,
                        height: 120.0,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            image: widget.logoImage,
                            fit: BoxFit.fill,
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 48,
                    ),
                    Card(
                      elevation: 2,
                      child: Container(
                        padding: const EdgeInsets.all(16),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              widget.subTitle,
                              style: const TextStyle(
                                fontFamily: 'sans-serif',
                                fontSize: 24,
                                color: Color(0xFF1A202C),
                                fontWeight: FontWeight.bold
                              ),
                              textAlign: TextAlign.left,
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Text(
                              widget.description,
                              style: TextStyle(
                                fontFamily: 'sans-serif',
                                fontSize: 14,
                                color: const Color(0xFF1A202C).withOpacity(0.5),
                              ),
                              textAlign: TextAlign.left,
                            ),
                            const SizedBox(
                              height: 16,
                            ),
                            TextFormField(
                              focusNode: widget.myFocusNodePhone,
                              controller: widget.phoneController,
                              decoration: InputDecoration(
                                labelText: "Nomor Telepon",
                                filled: true,
                                fillColor: Colors.grey.withOpacity(0.1),
                                focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10.0),
                                  borderSide: BorderSide(
                                    color: Colors.grey.withOpacity(0.1),
                                    width: 1.0,
                                  ),
                                ),
                                enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10.0),
                                  borderSide: BorderSide(
                                    color: Colors.grey.withOpacity(0.1),
                                    width: 1.0,
                                  ),
                                ),
                                disabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10.0),
                                  borderSide: BorderSide(
                                    color: Colors.grey.withOpacity(0.1),
                                    width: 1.0,
                                  ),
                                ),
                                labelStyle: TextStyle(
                                  fontFamily: 'sans-serif', 
                                  fontSize: 16, 
                                  color: Colors.black.withOpacity(0.5)
                                ),
                              ),
                              keyboardType: TextInputType.text,
                              style: const TextStyle(
                                fontFamily: 'sans-serif', 
                                fontSize: 16
                              ),
                            ),
                            const SizedBox(
                              height: 16,
                            ),
                            Container(
                              height: 60,
                              decoration: BoxDecoration(
                                boxShadow: const [
                                  BoxShadow(
                                    color: Colors.black26, 
                                    offset: Offset(0, 4), 
                                    blurRadius: 5.0
                                  )
                                ],
                                gradient: const LinearGradient(
                                  begin: Alignment.topLeft,
                                  end: Alignment.bottomRight,
                                  stops: [0.0, 1.0],
                                  colors: [
                                    Color(0xFFf4646a),
                                    Color(0xFFec65a4),
                                  ],
                                ),
                                color: const Color(0xFFec65a4),
                                borderRadius: BorderRadius.circular(10),
                              ),
                              child: TextButton(
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Text(
                                      widget.signInLabel,
                                      style: const TextStyle(
                                        fontFamily: 'sans-serif',
                                        fontSize: 18,
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold
                                      )
                                    )
                                  ],
                                ),
                                style: ButtonStyle(
                                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                    RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10.0),
                                      side: const BorderSide(color: Color(0xFFec65a4))
                                    )
                                  )
                                ),
                                onPressed: widget.onSignInPressed
                              ),
                            ),
                            const SizedBox(
                              height: 16,
                            ),
                            Container(
                              height: 60,
                              decoration: BoxDecoration(
                                boxShadow: const [
                                  BoxShadow(
                                    color: Colors.black26, 
                                    offset: Offset(0, 4), 
                                    blurRadius: 5.0
                                  )
                                ],
                                gradient: const LinearGradient(
                                  begin: Alignment.topLeft,
                                  end: Alignment.bottomRight,
                                  stops: [0.0, 1.0],
                                  colors: [
                                    Color(0xFFf4646a),
                                    Color(0xFFec65a4),
                                  ],
                                ),
                                color: const Color(0xFFec65a4),
                                borderRadius: BorderRadius.circular(10),
                              ),
                              child: TextButton(
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Text(
                                      widget.signUpLabel,
                                      style: const TextStyle(
                                        fontFamily: 'sans-serif',
                                        fontSize: 18,
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold
                                      )
                                    )
                                  ],
                                ),
                                style: ButtonStyle(
                                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                    RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10.0),
                                      side: const BorderSide(color: Color(0xFFec65a4))
                                    )
                                  )
                                ),
                                onPressed: widget.onSignUpPressed
                              ),
                            )
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            );
          },
        )
      ),
    );
  }
}