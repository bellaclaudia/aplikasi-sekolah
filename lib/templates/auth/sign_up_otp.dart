import 'package:flutter/material.dart';
import 'package:pinput/pin_put/pin_put.dart';

class SignUpOtpTemplate extends StatefulWidget {
  
  const SignUpOtpTemplate({
    Key key,
    this.bgImage,
    this.logoImage,
    this.scaffoldKey,
    this.title,
    this.description,
    this.pinPutFocusNode,
    this.pinPutController,
    this.onSignUpPressed,
    this.signUpLabel
  }) : super(key: key);

  final AssetImage bgImage;
  final AssetImage logoImage;
  final GlobalKey<ScaffoldState> scaffoldKey;
  final String title;
  final String description;
  final FocusNode pinPutFocusNode;
  final TextEditingController pinPutController;
  final Function onSignUpPressed;
  final String signUpLabel;

  @override
  State<SignUpOtpTemplate> createState() => _SignUpOtpTemplateState();
}

class _SignUpOtpTemplateState extends State<SignUpOtpTemplate> with SingleTickerProviderStateMixin {
  
  int _counter = 0;
  AnimationController _controller;
  int levelClock = 115;

  @override
  void initState() {
    _controller = AnimationController(
        vsync: this,
        duration: Duration(
            seconds:
                levelClock)
        );

    _controller.forward();
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        image: DecorationImage(
          image: widget.bgImage,
          fit: BoxFit.cover
        )
      ),
      padding: const EdgeInsets.only(top: 24, left: 16, right: 16),
      child: Scaffold(
        key: widget.scaffoldKey,
        backgroundColor: Colors.transparent,
        body: LayoutBuilder(
          builder: (context, viewportConstraints) {
            return SingleChildScrollView(
              child: ConstrainedBox(
                constraints: viewportConstraints.copyWith(
                  minHeight: viewportConstraints.maxHeight,
                  maxHeight: double.infinity,
                ),
                child: Flex(
                  direction: Axis.vertical,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      widget.title,
                      style: const TextStyle(
                        fontFamily: 'sans-serif',
                        fontSize: 30,
                        color: Color(0xFF1A202C),
                        fontWeight: FontWeight.bold
                      ),
                      textAlign: TextAlign.left,
                    ),
                    const SizedBox(
                      height: 24,
                    ),
                    Center(
                      child: Container(
                        width: 120.0,
                        height: 120.0,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            image: widget.logoImage,
                            fit: BoxFit.fill,
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 48,
                    ),
                    Card(
                      elevation: 2,
                      child: Container(
                        padding: const EdgeInsets.all(16),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Countdown(
                              animation: StepTween(
                                begin: levelClock, // THIS IS A USER ENTERED NUMBER
                                end: 0,
                              ).animate(_controller),
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Text(
                              widget.description,
                              style: TextStyle(
                                fontFamily: 'sans-serif',
                                fontSize: 14,
                                color: const Color(0xFF1A202C).withOpacity(0.5),
                              ),
                              textAlign: TextAlign.left,
                            ),
                            const SizedBox(
                              height: 16,
                            ),
                            PinPut(
                              fieldsCount: 6,
                              onSubmit: (String pin) {
                                
                              },
                              focusNode: widget.pinPutFocusNode,
                              controller: widget.pinPutController,
                              submittedFieldDecoration: BoxDecoration(
                                  border: Border.all(color: Colors.deepPurpleAccent),
                                  borderRadius: BorderRadius.circular(15.0),
                                ).copyWith(borderRadius: BorderRadius.circular(20.0),
                              ),
                              selectedFieldDecoration: BoxDecoration(
                                border: Border.all(color: Colors.deepPurpleAccent),
                                borderRadius: BorderRadius.circular(15.0),
                              ),
                              followingFieldDecoration: BoxDecoration(
                                border: Border.all(color: Colors.deepPurpleAccent),
                                borderRadius: BorderRadius.circular(15.0),
                              ).copyWith(
                                borderRadius: BorderRadius.circular(5.0),
                                border: Border.all(
                                  color: Colors.deepPurpleAccent.withOpacity(.5),
                                ),
                              ),
                            ),
                            const SizedBox(
                              height: 16,
                            ),
                            Container(
                              height: 60,
                              decoration: BoxDecoration(
                                boxShadow: const [
                                  BoxShadow(
                                    color: Colors.black26, 
                                    offset: Offset(0, 4), 
                                    blurRadius: 5.0
                                  )
                                ],
                                gradient: const LinearGradient(
                                  begin: Alignment.topLeft,
                                  end: Alignment.bottomRight,
                                  stops: [0.0, 1.0],
                                  colors: [
                                    Color(0xFFf4646a),
                                    Color(0xFFec65a4),
                                  ],
                                ),
                                color: const Color(0xFFec65a4),
                                borderRadius: BorderRadius.circular(10),
                              ),
                              child: TextButton(
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Text(
                                      widget.signUpLabel,
                                      style: const TextStyle(
                                        fontFamily: 'sans-serif',
                                        fontSize: 18,
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold
                                      )
                                    )
                                  ],
                                ),
                                style: ButtonStyle(
                                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                    RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10.0),
                                      side: const BorderSide(color: Color(0xFFec65a4))
                                    )
                                  )
                                ),
                                onPressed: widget.onSignUpPressed
                              ),
                            )
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            );
          },
        )
      ),
    );
  }
}

class Countdown extends AnimatedWidget {
  Countdown({Key key, this.animation}) : super(key: key, listenable: animation);
  Animation<int> animation;

  @override
  build(BuildContext context) {
    Duration clockTimer = Duration(seconds: animation.value);

    String timerText =
        '${clockTimer.inMinutes.remainder(60).toString()}:${clockTimer.inSeconds.remainder(60).toString().padLeft(2, '0')}';

    /*print('animation.value  ${animation.value} ');
    print('inMinutes ${clockTimer.inMinutes.toString()}');
    print('inSeconds ${clockTimer.inSeconds.toString()}');
    print('inSeconds.remainder ${clockTimer.inSeconds.remainder(60).toString()}');*/

    return Text(
      timerText,
      style: const TextStyle(
        fontFamily: 'sans-serif',
        fontSize: 24,
        color: Color(0xFF1A202C),
        fontWeight: FontWeight.bold
      ),
      textAlign: TextAlign.left,
    );
  }
}