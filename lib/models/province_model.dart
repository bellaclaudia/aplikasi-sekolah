class ProvinceModel {
  List<Province> province;
  ProvinceModel({this.province});

  factory ProvinceModel.fromJson(List<dynamic> parsedjson) {
    List<Province> province = List<Province>();
    province = parsedjson.map((e) => Province.fromJson(e)).toList();

    return ProvinceModel(province: province);
  }
}

class Province {
  final String id;
  final String name;

  Province({
    this.id,
    this.name,
  });

  factory Province.fromJson(Map<String, dynamic> json) {
    return Province(
      id: json['id'],
      name: json['name'],
    );
  }
}