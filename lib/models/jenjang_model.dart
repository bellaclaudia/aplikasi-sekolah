class JenjangModel {
  List<Jenjang> jenjang;
  JenjangModel({this.jenjang});

  factory JenjangModel.fromJson(List<dynamic> parsedjson) {
    List<Jenjang> jenjang = List<Jenjang>();
    jenjang = parsedjson.map((e) => Jenjang.fromJson(e)).toList();

    return JenjangModel(jenjang: jenjang);
  }
}

class Jenjang {
  final String id;
  final String name;

  Jenjang({
    this.id,
    this.name,
  });

  factory Jenjang.fromJson(Map<String, dynamic> json) {
    return Jenjang(
      id: json['id'],
      name: json['name'],
    );
  }
}
