class VillageModel {
  List<Village> village;
  VillageModel({this.village});

  factory VillageModel.fromJson(List<dynamic> parsedjson) {
    List<Village> village = List<Village>();
    village = parsedjson.map((e) => Village.fromJson(e)).toList();

    return VillageModel(village: village);
  }
}

class Village {
  final String id;
  final String district_id;
  final String name;

  Village({
    this.id,
    this.district_id,
    this.name,
  });

  factory Village.fromJson(Map<String, dynamic> json) {
    return Village(
      id: json['id'],
      district_id: json['district_id'],
      name: json['name'],
    );
  }
}