class CityModel {
  List<City> city;
  CityModel({this.city});

  factory CityModel.fromJson(List<dynamic> parsedjson) {
    List<City> city = List<City>();
    city = parsedjson.map((e) => City.fromJson(e)).toList();

    return CityModel(city: city);
  }
}

class City {
  final String id;
  final String province_id;
  final String name;

  City({
    this.id,
    this.province_id,
    this.name,
  });

  factory City.fromJson(Map<String, dynamic> json) {
    return City(
      id: json['id'],
      province_id: json['province_id'],
      name: json['name'],
    );
  }
}