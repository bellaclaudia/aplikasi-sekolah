class DistrictModel {
  List<District> district;
  DistrictModel({this.district});

  factory DistrictModel.fromJson(List<dynamic> parsedjson) {
    List<District> district = List<District>();
    district = parsedjson.map((e) => District.fromJson(e)).toList();

    return DistrictModel(district: district);
  }
}

class District {
  final String id;
  final String regency_id;
  final String name;

  District({
    this.id,
    this.regency_id,
    this.name,
  });

  factory District.fromJson(Map<String, dynamic> json) {
    return District(
      id: json['id'],
      regency_id: json['regency_id'],
      name: json['name'],
    );
  }
}