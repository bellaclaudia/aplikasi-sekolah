// ignore_for_file: avoid_print

import 'dart:io';

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:aplikasi_sekolah/welcome/splash.dart';
import 'package:get_it/get_it.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:supabase/supabase.dart';

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  await Firebase.initializeApp();
  print('Handling a background message ${message.messageId}');
}

void main() async {
  await dotenv.load(fileName: ".env");
  GetIt locator = GetIt.instance;
  locator.registerSingleton<SupabaseClient>(
      SupabaseClient(dotenv.env['SB_URL'], dotenv.env['SB_ANON']));
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);
  if (await _checkPermission()) {
    runApp(const MyApp());
  }
}

Future<bool> _checkPermission() async {
  if (Platform.isAndroid) {
    final status = await Permission.storage.status;
    if (status != PermissionStatus.granted) {
      final result = await Permission.storage.request();
      if (result == PermissionStatus.granted) {
        return true;
      }
    } else {
      return true;
    }
  } else {
    return true;
  }
  return false;
}

class MyApp extends StatefulWidget {
  const MyApp({Key key}) : super(key: key);

  @override
  MyAppState createState() => MyAppState();
}

class MyAppState extends State<MyApp> {
  FirebaseMessaging _firebaseMessaging;
  SharedPreferences sharedPreferences;
  Widget screen;

  @override
  void initState() {
    _firebaseMessaging = FirebaseMessaging.instance;
    _firebaseMessaging.getToken().then((token) async {
      sharedPreferences = await SharedPreferences.getInstance();
      sharedPreferences.setString("token", token);
      sharedPreferences.commit();
      print(token);
    });

    WidgetsBinding.instance.addPostFrameCallback((_) {
      init();
    });

    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
    ));

    super.initState();
  }

  void init() async {
    sharedPreferences = await SharedPreferences.getInstance();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
            brightness: Brightness.light,
            primaryColor: const Color(0xFFFFFFFF),
            focusColor: const Color(0xFF5C499E),
            accentColor: const Color(0xFF5C499E),
            fontFamily: 'Aller'),
        home: const Splash());
  }
}
