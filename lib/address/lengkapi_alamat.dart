// ignore_for_file: avoid_print, void_checks

import 'dart:async';
import 'package:aplikasi_sekolah/libraries/pull_to_refresh.dart';
import 'package:aplikasi_sekolah/libraries/searchable_dropdown.dart';
import 'package:aplikasi_sekolah/models/city_model.dart';
import 'package:aplikasi_sekolah/models/district_model.dart';
import 'package:aplikasi_sekolah/models/list_dropdown_model.dart';
import 'package:aplikasi_sekolah/models/province_model.dart';
import 'package:aplikasi_sekolah/models/village_model.dart';
import 'package:aplikasi_sekolah/models/jenjang_model.dart';

import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:supabase/supabase.dart';

class LengkapiAlamat extends StatefulWidget {
  const LengkapiAlamat({Key key}) : super(key: key);
  @override
  LengkapiAlamatState createState() => LengkapiAlamatState();
}

class LengkapiAlamatState extends State<LengkapiAlamat>
    with TickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  ScrollController scrollController = ScrollController();
  Animation<double> topBarAnimation;
  double topBarOpacity = 0.0;

  SharedPreferences sharedPreferences;
  String apiToken;
  final RefreshController _refreshController =
      RefreshController(initialRefresh: false);
  bool rfrsh = true;
  String url;

  AnimationController animationControllers;
  Map<String, dynamic> responseSharedPref;
  String tokenFirebase;

  final TextEditingController alamatController = TextEditingController();
  final FocusNode myFocusNodeAlamat = FocusNode();

  final TextEditingController rtController = TextEditingController();
  final FocusNode myFocusNodeRt = FocusNode();

  final TextEditingController rwController = TextEditingController();
  final FocusNode myFocusNodeRw = FocusNode();

  final TextEditingController sekolahController = TextEditingController();
  final FocusNode myFocusNodeSekolah = FocusNode();

  Future<ProvinceModel> _future;
  Future<CityModel> _futureCity;
  Future<DistrictModel> _futureDistrict;
  Future<VillageModel> _futureVillage;
  Future<JenjangModel> _futureJenjang;

  List<ListDropdownModel> listProvince = [];
  String _mySelectionProvince;
  Widget ignoreProvince;

  List<ListDropdownModel> listCity = [];
  String _mySelectionCity;
  Widget ignoreCity;

  List<ListDropdownModel> listDistrict = [];
  String _mySelectionDistrict;
  Widget ignoreDistrict;

  List<ListDropdownModel> listVillage = [];
  String _mySelectionVillage;
  Widget ignoreVillage;

  List<ListDropdownModel> listJenjang = [];
  String _mySelectionJenjang;
  Widget ignoreJenjang;

  @override
  void initState() {
    listProvince.clear();
    listCity.clear();
    listDistrict.clear();
    listVillage.clear();
    listJenjang.clear();
    _future = makeRequest();
    animationControllers = AnimationController(
        duration: const Duration(milliseconds: 1300), vsync: this);
    topBarAnimation = Tween<double>(begin: 0.0, end: 1.0).animate(
        CurvedAnimation(
            parent: animationControllers,
            curve: const Interval(0, 0.5, curve: Curves.fastOutSlowIn)));

    scrollController.addListener(() {
      if (scrollController.offset >= 24) {
        if (topBarOpacity != 1.0) {
          setState(() {
            topBarOpacity = 1.0;
          });
        }
      } else if (scrollController.offset <= 24 &&
          scrollController.offset >= 0) {
        if (topBarOpacity != scrollController.offset / 24) {
          setState(() {
            topBarOpacity = scrollController.offset / 24;
          });
        }
      } else if (scrollController.offset <= 0) {
        if (topBarOpacity != 0.0) {
          setState(() {
            topBarOpacity = 0.0;
          });
        }
      }
    });
    super.initState();
  }

  Future<ProvinceModel> makeRequest() async {
    sharedPreferences = await SharedPreferences.getInstance();
    tokenFirebase = sharedPreferences.getString("token");
    try {
      final getProvince = await GetIt.instance<SupabaseClient>()
          .from("reg_provinces")
          .select()
          .execute();
      List<dynamic> value = getProvince.data;
      setState(() {
        listProvince.clear();
        listCity.clear();
        listDistrict.clear();
        listVillage.clear();
        listJenjang.clear();

        for (int i = 0; i < value.length; i++) {
          ListDropdownModel prov = ListDropdownModel(
              name: value[i]["name"].toString(), id: value[i]["id"].toString());
          listProvince.add(prov);
        }

        ignoreProvince = IgnorePointer(
            ignoring: false,
            child: Container(
              padding: const EdgeInsets.only(top: 5, bottom: 5),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  border: Border.all(color: Colors.grey.withOpacity(0.1)),
                  color: Colors.grey.withOpacity(0.1)),
              child: StatefulBuilder(builder: (context, setStates) {
                return SearchableDropdown.single(
                  items: listProvince.map((exNum) {
                    return (DropdownMenuItem(
                        child: Container(
                          padding: const EdgeInsets.only(left: 0),
                          child: Text(exNum.name.toString(),
                              style: const TextStyle(
                                fontFamily: 'Aller',
                                color: Colors.black,
                                fontSize: 14.0,
                              ),
                              textAlign: TextAlign.left),
                        ),
                        value: exNum.id));
                  }).toList(),
                  value: _mySelectionProvince,
                  hint: "Pilih Provinsi",
                  searchHint: "Cari Provinsi",
                  onChanged: (val) {
                    setStates(() {
                      _mySelectionProvince = val;
                      _futureCity = makeRequestCity();
                    });
                  },
                  dialogBox: true,
                  isExpanded: true,
                );
              }),
            ));

        ListDropdownModel city =
            ListDropdownModel(name: "Pilih Provinsi", id: "0");
        listCity.add(city);
        ignoreCity = IgnorePointer(
            ignoring: false,
            child: Container(
              padding: const EdgeInsets.only(top: 5, bottom: 5),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  border: Border.all(color: Colors.grey.withOpacity(0.1)),
                  color: Colors.grey.withOpacity(0.1)),
              child: StatefulBuilder(builder: (context, setStates) {
                return SearchableDropdown.single(
                  items: listCity.map((exNum) {
                    return (DropdownMenuItem(
                        child: Container(
                          padding: const EdgeInsets.only(left: 0),
                          child: Text(exNum.name.toString(),
                              style: const TextStyle(
                                fontFamily: 'Aller',
                                color: Colors.black,
                                fontSize: 14.0,
                              ),
                              textAlign: TextAlign.left),
                        ),
                        value: exNum.id));
                  }).toList(),
                  value: _mySelectionCity,
                  hint: "Pilih Kota",
                  searchHint: "Cari Kota",
                  onChanged: (val) {
                    setStates(() {
                      _mySelectionCity = val;
                      _futureDistrict = makeRequestDistrict();
                    });
                  },
                  dialogBox: true,
                  isExpanded: true,
                );
              }),
            ));

        ListDropdownModel district =
            ListDropdownModel(name: "Pilih Kota", id: "0");
        listDistrict.add(district);
        ignoreDistrict = IgnorePointer(
            ignoring: false,
            child: Container(
              padding: const EdgeInsets.only(top: 5, bottom: 5),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  border: Border.all(color: Colors.grey.withOpacity(0.1)),
                  color: Colors.grey.withOpacity(0.1)),
              child: StatefulBuilder(builder: (context, setStates) {
                return SearchableDropdown.single(
                  items: listDistrict.map((exNum) {
                    return (DropdownMenuItem(
                        child: Container(
                          padding: const EdgeInsets.only(left: 0),
                          child: Text(exNum.name.toString(),
                              style: const TextStyle(
                                fontFamily: 'Aller',
                                color: Colors.black,
                                fontSize: 14.0,
                              ),
                              textAlign: TextAlign.left),
                        ),
                        value: exNum.id));
                  }).toList(),
                  value: _mySelectionDistrict,
                  hint: "Pilih Kecamatan",
                  searchHint: "Cari Kecamatan",
                  onChanged: (val) {
                    setStates(() {
                      _mySelectionDistrict = val;
                      _futureVillage = makeRequestVillage();
                    });
                  },
                  dialogBox: true,
                  isExpanded: true,
                );
              }),
            ));

        ListDropdownModel village =
            ListDropdownModel(name: "Pilih Kecamatan", id: "0");
        listVillage.add(village);
        ignoreVillage = IgnorePointer(
            ignoring: false,
            child: Container(
              padding: const EdgeInsets.only(top: 5, bottom: 5),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  border: Border.all(color: Colors.grey.withOpacity(0.1)),
                  color: Colors.grey.withOpacity(0.1)),
              child: StatefulBuilder(builder: (context, setStates) {
                return SearchableDropdown.single(
                  items: listVillage.map((exNum) {
                    return (DropdownMenuItem(
                        child: Container(
                          padding: const EdgeInsets.only(left: 0),
                          child: Text(exNum.name.toString(),
                              style: const TextStyle(
                                fontFamily: 'Aller',
                                color: Colors.black,
                                fontSize: 14.0,
                              ),
                              textAlign: TextAlign.left),
                        ),
                        value: exNum.id));
                  }).toList(),
                  value: _mySelectionVillage,
                  hint: "Pilih Kelurahan",
                  searchHint: "Cari Kelurahan",
                  onChanged: (val) {
                    setStates(() {
                      _mySelectionVillage = val;
                    });
                  },
                  dialogBox: true,
                  isExpanded: true,
                );
              }),
            ));
      });
      if (!rfrsh) {
        _refreshController.loadComplete();
      } else {
        _refreshController.refreshCompleted();
      }
      return ProvinceModel.fromJson(getProvince.data);
    } on Exception catch (exception) {
      print("debugs exception " + exception.toString());
    } catch (error) {
      print("debugs error " + error.toString());
    }
  }

  Future<CityModel> makeRequestCity() async {
    sharedPreferences = await SharedPreferences.getInstance();
    tokenFirebase = sharedPreferences.getString("token");
    try {
      final getCity = await GetIt.instance<SupabaseClient>()
          .from("reg_regencies")
          .select()
          .eq("province_id", _mySelectionProvince)
          .execute();
      List<dynamic> value = getCity.data;
      setState(() {
        listCity.clear();
        listDistrict.clear();
        listVillage.clear();
        for (int i = 0; i < value.length; i++) {
          ListDropdownModel city = ListDropdownModel(
              name: value[i]["name"].toString(), id: value[i]["id"].toString());
          listCity.add(city);
        }
        ignoreCity = IgnorePointer(
            ignoring: false,
            child: Container(
              padding: const EdgeInsets.only(top: 5, bottom: 5),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  border: Border.all(color: Colors.grey.withOpacity(0.1)),
                  color: Colors.grey.withOpacity(0.1)),
              child: StatefulBuilder(builder: (context, setStates) {
                return SearchableDropdown.single(
                  items: listCity.map((exNum) {
                    return (DropdownMenuItem(
                        child: Container(
                          padding: const EdgeInsets.only(left: 0),
                          child: Text(exNum.name.toString(),
                              style: const TextStyle(
                                fontFamily: 'Aller',
                                color: Colors.black,
                                fontSize: 14.0,
                              ),
                              textAlign: TextAlign.left),
                        ),
                        value: exNum.id));
                  }).toList(),
                  value: _mySelectionCity,
                  hint: "Pilih Kota",
                  searchHint: "Cari Kota",
                  onChanged: (val) {
                    setStates(() {
                      _mySelectionCity = val;
                      _futureDistrict = makeRequestDistrict();
                    });
                  },
                  dialogBox: true,
                  isExpanded: true,
                );
              }),
            ));

        ListDropdownModel district =
            ListDropdownModel(name: "Pilih Kota", id: "0");
        listDistrict.add(district);
        ignoreDistrict = IgnorePointer(
            ignoring: false,
            child: Container(
              padding: const EdgeInsets.only(top: 5, bottom: 5),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  border: Border.all(color: Colors.grey.withOpacity(0.1)),
                  color: Colors.grey.withOpacity(0.1)),
              child: StatefulBuilder(builder: (context, setStates) {
                return SearchableDropdown.single(
                  items: listDistrict.map((exNum) {
                    return (DropdownMenuItem(
                        child: Container(
                          padding: const EdgeInsets.only(left: 0),
                          child: Text(exNum.name.toString(),
                              style: const TextStyle(
                                fontFamily: 'Aller',
                                color: Colors.black,
                                fontSize: 14.0,
                              ),
                              textAlign: TextAlign.left),
                        ),
                        value: exNum.id));
                  }).toList(),
                  value: _mySelectionDistrict,
                  hint: "Pilih Kecamatan",
                  searchHint: "Cari Kecamatan",
                  onChanged: (val) {
                    setStates(() {
                      _mySelectionDistrict = val;
                      _futureVillage = makeRequestVillage();
                    });
                  },
                  dialogBox: true,
                  isExpanded: true,
                );
              }),
            ));

        ListDropdownModel village =
            ListDropdownModel(name: "Pilih Kecamatan", id: "0");
        listVillage.add(village);
        ignoreVillage = IgnorePointer(
            ignoring: false,
            child: Container(
              padding: const EdgeInsets.only(top: 5, bottom: 5),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  border: Border.all(color: Colors.grey.withOpacity(0.1)),
                  color: Colors.grey.withOpacity(0.1)),
              child: StatefulBuilder(builder: (context, setStates) {
                return SearchableDropdown.single(
                  items: listVillage.map((exNum) {
                    return (DropdownMenuItem(
                        child: Container(
                          padding: const EdgeInsets.only(left: 0),
                          child: Text(exNum.name.toString(),
                              style: const TextStyle(
                                fontFamily: 'Aller',
                                color: Colors.black,
                                fontSize: 14.0,
                              ),
                              textAlign: TextAlign.left),
                        ),
                        value: exNum.id));
                  }).toList(),
                  value: _mySelectionVillage,
                  hint: "Pilih Kelurahan",
                  searchHint: "Cari Kelurahan",
                  onChanged: (val) {
                    setStates(() {
                      _mySelectionVillage = val;
                    });
                  },
                  dialogBox: true,
                  isExpanded: true,
                );
              }),
            ));
      });
      if (!rfrsh) {
        _refreshController.loadComplete();
      } else {
        _refreshController.refreshCompleted();
      }
      return CityModel.fromJson(getCity.data);
    } on Exception catch (exception) {
      print("debugs exception " + exception.toString());
    } catch (error) {
      print("debugs error " + error.toString());
    }
  }

  Future<DistrictModel> makeRequestDistrict() async {
    sharedPreferences = await SharedPreferences.getInstance();
    tokenFirebase = sharedPreferences.getString("token");
    try {
      final getDistrict = await GetIt.instance<SupabaseClient>()
          .from("reg_districts")
          .select()
          .eq("regency_id", _mySelectionCity)
          .execute();
      List<dynamic> value = getDistrict.data;
      setState(() {
        listDistrict.clear();
        listVillage.clear();
        for (int i = 0; i < value.length; i++) {
          ListDropdownModel district = ListDropdownModel(
              name: value[i]["name"].toString(), id: value[i]["id"].toString());
          listDistrict.add(district);
        }
        ignoreDistrict = IgnorePointer(
            ignoring: false,
            child: Container(
              padding: const EdgeInsets.only(top: 5, bottom: 5),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  border: Border.all(color: Colors.grey.withOpacity(0.1)),
                  color: Colors.grey.withOpacity(0.1)),
              child: StatefulBuilder(builder: (context, setStates) {
                return SearchableDropdown.single(
                  items: listDistrict.map((exNum) {
                    return (DropdownMenuItem(
                        child: Container(
                          padding: const EdgeInsets.only(left: 0),
                          child: Text(exNum.name.toString(),
                              style: const TextStyle(
                                fontFamily: 'Aller',
                                color: Colors.black,
                                fontSize: 14.0,
                              ),
                              textAlign: TextAlign.left),
                        ),
                        value: exNum.id));
                  }).toList(),
                  value: _mySelectionDistrict,
                  hint: "Pilih Kecamatan",
                  searchHint: "Cari Kecamatan",
                  onChanged: (val) {
                    setStates(() {
                      _mySelectionDistrict = val;
                      _futureVillage = makeRequestVillage();
                    });
                  },
                  dialogBox: true,
                  isExpanded: true,
                );
              }),
            ));

        ListDropdownModel village =
            ListDropdownModel(name: "Pilih Kecamatan", id: "0");
        listVillage.add(village);
        ignoreVillage = IgnorePointer(
            ignoring: false,
            child: Container(
              padding: const EdgeInsets.only(top: 5, bottom: 5),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  border: Border.all(color: Colors.grey.withOpacity(0.1)),
                  color: Colors.grey.withOpacity(0.1)),
              child: StatefulBuilder(builder: (context, setStates) {
                return SearchableDropdown.single(
                  items: listVillage.map((exNum) {
                    return (DropdownMenuItem(
                        child: Container(
                          padding: const EdgeInsets.only(left: 0),
                          child: Text(exNum.name.toString(),
                              style: const TextStyle(
                                fontFamily: 'Aller',
                                color: Colors.black,
                                fontSize: 14.0,
                              ),
                              textAlign: TextAlign.left),
                        ),
                        value: exNum.id));
                  }).toList(),
                  value: _mySelectionVillage,
                  hint: "Pilih Kelurahan",
                  searchHint: "Cari Kelurahan",
                  onChanged: (val) {
                    setStates(() {
                      _mySelectionVillage = val;
                    });
                  },
                  dialogBox: true,
                  isExpanded: true,
                );
              }),
            ));
      });
      if (!rfrsh) {
        _refreshController.loadComplete();
      } else {
        _refreshController.refreshCompleted();
      }
      return DistrictModel.fromJson(getDistrict.data);
    } on Exception catch (exception) {
      print("debugs exception " + exception.toString());
    } catch (error) {
      print("debugs error " + error.toString());
    }
  }

  Future<VillageModel> makeRequestVillage() async {
    sharedPreferences = await SharedPreferences.getInstance();
    tokenFirebase = sharedPreferences.getString("token");
    try {
      final getVillage = await GetIt.instance<SupabaseClient>()
          .from("reg_villages")
          .select()
          .eq("district_id", _mySelectionDistrict)
          .execute();
      List<dynamic> value = getVillage.data;
      setState(() {
        listVillage.clear();
        for (int i = 0; i < value.length; i++) {
          ListDropdownModel village = ListDropdownModel(
              name: value[i]["name"].toString(), id: value[i]["id"].toString());
          listVillage.add(village);
        }
        ignoreVillage = IgnorePointer(
            ignoring: false,
            child: Container(
              padding: const EdgeInsets.only(top: 5, bottom: 5),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  border: Border.all(color: Colors.grey.withOpacity(0.1)),
                  color: Colors.grey.withOpacity(0.1)),
              child: StatefulBuilder(builder: (context, setStates) {
                return SearchableDropdown.single(
                  items: listVillage.map((exNum) {
                    return (DropdownMenuItem(
                        child: Container(
                          padding: const EdgeInsets.only(left: 0),
                          child: Text(exNum.name.toString(),
                              style: const TextStyle(
                                fontFamily: 'Aller',
                                color: Colors.black,
                                fontSize: 14.0,
                              ),
                              textAlign: TextAlign.left),
                        ),
                        value: exNum.id));
                  }).toList(),
                  value: _mySelectionVillage,
                  hint: "Pilih Kelurahan",
                  searchHint: "Cari Kelurahan",
                  onChanged: (val) {
                    setStates(() {
                      _mySelectionVillage = val;
                    });
                  },
                  dialogBox: true,
                  isExpanded: true,
                );
              }),
            ));
      });
      if (!rfrsh) {
        _refreshController.loadComplete();
      } else {
        _refreshController.refreshCompleted();
      }
      return VillageModel.fromJson(getVillage.data);
    } on Exception catch (exception) {
      print("debugs exception " + exception.toString());
    } catch (error) {
      print("debugs error " + error.toString());
    }
  }

  Future<JenjangModel> makeRequestJenjang() async {
    sharedPreferences = await SharedPreferences.getInstance();
    tokenFirebase = sharedPreferences.getString("token");
    try {
      final getJenjang = await GetIt.instance<SupabaseClient>()
          .from("reg_jenjang")
          .select()
          .execute();
      List<dynamic> value = getJenjang.data;
      setState(() {
        listJenjang.clear();
        for (int i = 0; i < value.length; i++) {
          ListDropdownModel jenjang = ListDropdownModel(
              name: value[i]["name"].toString(), id: value[i]["id"].toString());
          listJenjang.add(jenjang);
        }
        ignoreJenjang = IgnorePointer(
            ignoring: false,
            child: Container(
              padding: const EdgeInsets.only(top: 5, bottom: 5),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  border: Border.all(color: Colors.grey.withOpacity(0.1)),
                  color: Colors.grey.withOpacity(0.1)),
              child: StatefulBuilder(builder: (context, setStates) {
                return SearchableDropdown.single(
                  items: listJenjang.map((exNum) {
                    return (DropdownMenuItem(
                        child: Container(
                          padding: const EdgeInsets.only(left: 0),
                          child: Text(exNum.name.toString(),
                              style: const TextStyle(
                                fontFamily: 'Aller',
                                color: Colors.black,
                                fontSize: 14.0,
                              ),
                              textAlign: TextAlign.left),
                        ),
                        value: exNum.id));
                  }).toList(),
                  value: _mySelectionJenjang,
                  hint: "Pilih Jenjang",
                  searchHint: "Cari Jenjang",
                  onChanged: (val) {
                    setStates(() {
                      _mySelectionJenjang = val;
                    });
                  },
                  dialogBox: true,
                  isExpanded: true,
                );
              }),
            ));
      });
      if (!rfrsh) {
        _refreshController.loadComplete();
      } else {
        _refreshController.refreshCompleted();
      }
      return JenjangModel.fromJson(getJenjang.data);
    } on Exception catch (exception) {
      print("debugs exception " + exception.toString());
    } catch (error) {
      print("debugs error " + error.toString());
    }
  }

  void onSubmitPressed() {
    print(_mySelectionProvince.toString());
  }

  Future<bool> _onBackPressed() async {}

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: _onBackPressed,
        child: Scaffold(
            key: _scaffoldKey,
            body: FutureBuilder<ProvinceModel>(
              future: _future,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return SmartRefresher(
                      enablePullDown: true,
                      enablePullUp: true,
                      header: const WaterDropHeader(),
                      footer: CustomFooter(
                        builder: (BuildContext context, LoadStatus mode) {
                          Widget body;
                          if (mode == LoadStatus.idle) {
                            // body =  const Text("pull up load");
                          } else if (mode == LoadStatus.loading) {
                            // body =  const CupertinoActivityIndicator();
                          } else if (mode == LoadStatus.failed) {
                            // body = const Text("Load Failed!Click retry!");
                          } else {
                            // body = const Text("No more Data");
                          }
                          return SizedBox(
                            height: 55.0,
                            child: Center(child: body),
                          );
                        },
                      ),
                      controller: _refreshController,
                      onRefresh: () {
                        setState(() {
                          rfrsh = true;
                          _future = makeRequest();
                        });
                      },
                      onLoading: () {
                        setState(() {
                          rfrsh = false;
                        });
                      },
                      child: Container(
                        decoration: const BoxDecoration(
                            color: Colors.white,
                            image: DecorationImage(
                                image: AssetImage("assets/bg.png"),
                                fit: BoxFit.cover)),
                        padding:
                            const EdgeInsets.only(top: 24, left: 16, right: 16),
                        child: LayoutBuilder(
                          builder: (context, viewportConstraints) {
                            return SingleChildScrollView(
                              child: ConstrainedBox(
                                constraints: viewportConstraints.copyWith(
                                  minHeight: viewportConstraints.maxHeight,
                                  maxHeight: double.infinity,
                                ),
                                child: Flex(
                                  direction: Axis.vertical,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    const Text(
                                      "Lengkapi Alamat",
                                      style: TextStyle(
                                          fontFamily: 'sans-serif',
                                          fontSize: 30,
                                          color: Color(0xFF1A202C),
                                          fontWeight: FontWeight.bold),
                                      textAlign: TextAlign.left,
                                    ),
                                    const SizedBox(
                                      height: 48,
                                    ),
                                    Card(
                                      elevation: 2,
                                      child: Container(
                                        padding: const EdgeInsets.all(16),
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            const Text(
                                              "Join our team!",
                                              style: TextStyle(
                                                  fontFamily: 'sans-serif',
                                                  fontSize: 24,
                                                  color: Color(0xFF1A202C),
                                                  fontWeight: FontWeight.bold),
                                              textAlign: TextAlign.left,
                                            ),
                                            const SizedBox(
                                              height: 10,
                                            ),
                                            Text(
                                              "We’re looking for amazing engineers just like you! Become a part of our rockstar engineering team and skyrocket your career!",
                                              style: TextStyle(
                                                fontFamily: 'sans-serif',
                                                fontSize: 14,
                                                color: const Color(0xFF1A202C)
                                                    .withOpacity(0.5),
                                              ),
                                              textAlign: TextAlign.left,
                                            ),
                                            const SizedBox(
                                              height: 16,
                                            ),
                                            TextFormField(
                                              focusNode: myFocusNodeAlamat,
                                              controller: alamatController,
                                              decoration: InputDecoration(
                                                labelText: "Alamat",
                                                filled: true,
                                                fillColor: Colors.grey
                                                    .withOpacity(0.1),
                                                focusedBorder:
                                                    OutlineInputBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          10.0),
                                                  borderSide: BorderSide(
                                                    color: Colors.grey
                                                        .withOpacity(0.1),
                                                    width: 1.0,
                                                  ),
                                                ),
                                                enabledBorder:
                                                    OutlineInputBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          10.0),
                                                  borderSide: BorderSide(
                                                    color: Colors.grey
                                                        .withOpacity(0.1),
                                                    width: 1.0,
                                                  ),
                                                ),
                                                disabledBorder:
                                                    OutlineInputBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          10.0),
                                                  borderSide: BorderSide(
                                                    color: Colors.grey
                                                        .withOpacity(0.1),
                                                    width: 1.0,
                                                  ),
                                                ),
                                                labelStyle: TextStyle(
                                                    fontFamily: 'sans-serif',
                                                    fontSize: 16,
                                                    color: Colors.black
                                                        .withOpacity(0.5)),
                                              ),
                                              keyboardType:
                                                  TextInputType.multiline,
                                              maxLines: 3,
                                              minLines: 3,
                                              style: const TextStyle(
                                                  fontFamily: 'sans-serif',
                                                  fontSize: 16),
                                            ),
                                            const SizedBox(
                                              height: 16,
                                            ),
                                            Container(
                                              padding: const EdgeInsets.only(
                                                  left: 0, right: 0),
                                              child: Row(
                                                children: [
                                                  Expanded(
                                                    child: ignoreProvince,
                                                  )
                                                ],
                                              ),
                                            ),
                                            const SizedBox(
                                              height: 16,
                                            ),
                                            Container(
                                              padding: const EdgeInsets.only(
                                                  left: 0, right: 0),
                                              child: Row(
                                                children: [
                                                  Expanded(
                                                    child: ignoreCity,
                                                  )
                                                ],
                                              ),
                                            ),
                                            const SizedBox(
                                              height: 16,
                                            ),
                                            Container(
                                              padding: const EdgeInsets.only(
                                                  left: 0, right: 0),
                                              child: Row(
                                                children: [
                                                  Expanded(
                                                    child: ignoreDistrict,
                                                  )
                                                ],
                                              ),
                                            ),
                                            const SizedBox(
                                              height: 5,
                                            ),
                                            Container(
                                              padding: const EdgeInsets.only(
                                                  left: 0, right: 0),
                                              child: Row(
                                                children: [
                                                  Expanded(
                                                    child: ignoreJenjang,
                                                  )
                                                ],
                                              ),
                                            ),
                                            const SizedBox(
                                              height: 16,
                                            ),
                                            Container(
                                              padding: const EdgeInsets.only(
                                                  left: 0, right: 0),
                                              child: Row(
                                                children: [
                                                  Expanded(
                                                    child: ignoreVillage,
                                                  )
                                                ],
                                              ),
                                            ),
                                            const SizedBox(
                                              height: 16,
                                            ),
                                            Row(
                                              children: [
                                                Expanded(
                                                  child: TextFormField(
                                                    focusNode: myFocusNodeRt,
                                                    controller: rtController,
                                                    decoration: InputDecoration(
                                                      labelText: "RT",
                                                      filled: true,
                                                      fillColor: Colors.grey
                                                          .withOpacity(0.1),
                                                      focusedBorder:
                                                          OutlineInputBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(10.0),
                                                        borderSide: BorderSide(
                                                          color: Colors.grey
                                                              .withOpacity(0.1),
                                                          width: 1.0,
                                                        ),
                                                      ),
                                                      enabledBorder:
                                                          OutlineInputBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(10.0),
                                                        borderSide: BorderSide(
                                                          color: Colors.grey
                                                              .withOpacity(0.1),
                                                          width: 1.0,
                                                        ),
                                                      ),
                                                      disabledBorder:
                                                          OutlineInputBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(10.0),
                                                        borderSide: BorderSide(
                                                          color: Colors.grey
                                                              .withOpacity(0.1),
                                                          width: 1.0,
                                                        ),
                                                      ),
                                                      labelStyle: TextStyle(
                                                          fontFamily:
                                                              'sans-serif',
                                                          fontSize: 16,
                                                          color: Colors.black
                                                              .withOpacity(
                                                                  0.5)),
                                                    ),
                                                    keyboardType:
                                                        TextInputType.text,
                                                    style: const TextStyle(
                                                        fontFamily:
                                                            'sans-serif',
                                                        fontSize: 16),
                                                  ),
                                                ),
                                                const SizedBox(
                                                  width: 5,
                                                ),
                                                Expanded(
                                                  child: TextFormField(
                                                    focusNode: myFocusNodeRw,
                                                    controller: rwController,
                                                    decoration: InputDecoration(
                                                      labelText: "RW",
                                                      filled: true,
                                                      fillColor: Colors.grey
                                                          .withOpacity(0.1),
                                                      focusedBorder:
                                                          OutlineInputBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(10.0),
                                                        borderSide: BorderSide(
                                                          color: Colors.grey
                                                              .withOpacity(0.1),
                                                          width: 1.0,
                                                        ),
                                                      ),
                                                      enabledBorder:
                                                          OutlineInputBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(10.0),
                                                        borderSide: BorderSide(
                                                          color: Colors.grey
                                                              .withOpacity(0.1),
                                                          width: 1.0,
                                                        ),
                                                      ),
                                                      disabledBorder:
                                                          OutlineInputBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(10.0),
                                                        borderSide: BorderSide(
                                                          color: Colors.grey
                                                              .withOpacity(0.1),
                                                          width: 1.0,
                                                        ),
                                                      ),
                                                      labelStyle: TextStyle(
                                                          fontFamily:
                                                              'sans-serif',
                                                          fontSize: 16,
                                                          color: Colors.black
                                                              .withOpacity(
                                                                  0.5)),
                                                    ),
                                                    keyboardType:
                                                        TextInputType.text,
                                                    style: const TextStyle(
                                                        fontFamily:
                                                            'sans-serif',
                                                        fontSize: 16),
                                                  ),
                                                ),
                                              ],
                                            ),
                                            const SizedBox(
                                              height: 16,
                                            ),
                                            Row(
                                              children: [
                                                Expanded(
                                                  child: TextFormField(
                                                    focusNode:
                                                        myFocusNodeSekolah,
                                                    controller:
                                                        sekolahController,
                                                    decoration: InputDecoration(
                                                      labelText: "Asal Sekolah",
                                                      filled: true,
                                                      fillColor: Colors.grey
                                                          .withOpacity(0.1),
                                                      focusedBorder:
                                                          OutlineInputBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(10.0),
                                                        borderSide: BorderSide(
                                                          color: Colors.grey
                                                              .withOpacity(0.1),
                                                          width: 1.0,
                                                        ),
                                                      ),
                                                      enabledBorder:
                                                          OutlineInputBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(10.0),
                                                        borderSide: BorderSide(
                                                          color: Colors.grey
                                                              .withOpacity(0.1),
                                                          width: 1.0,
                                                        ),
                                                      ),
                                                      disabledBorder:
                                                          OutlineInputBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(10.0),
                                                        borderSide: BorderSide(
                                                          color: Colors.grey
                                                              .withOpacity(0.1),
                                                          width: 1.0,
                                                        ),
                                                      ),
                                                      labelStyle: TextStyle(
                                                          fontFamily:
                                                              'sans-serif',
                                                          fontSize: 16,
                                                          color: Colors.black
                                                              .withOpacity(
                                                                  0.5)),
                                                    ),
                                                    keyboardType:
                                                        TextInputType.text,
                                                    style: const TextStyle(
                                                        fontFamily:
                                                            'sans-serif',
                                                        fontSize: 16),
                                                  ),
                                                ),
                                              ],
                                            ),
                                            const SizedBox(
                                              height: 10,
                                            ),
                                            Container(
                                              padding: const EdgeInsets.only(
                                                  left: 0, right: 0),
                                              child: Row(
                                                children: [
                                                  Expanded(
                                                    child: ignoreJenjang,
                                                  )
                                                ],
                                              ),
                                            ),
                                            const SizedBox(
                                              height: 16,
                                            ),
                                            Container(
                                              height: 60,
                                              decoration: BoxDecoration(
                                                boxShadow: const [
                                                  BoxShadow(
                                                      color: Colors.black26,
                                                      offset: Offset(0, 4),
                                                      blurRadius: 5.0)
                                                ],
                                                gradient: const LinearGradient(
                                                  begin: Alignment.topLeft,
                                                  end: Alignment.bottomRight,
                                                  stops: [0.0, 1.0],
                                                  colors: [
                                                    Color(0xFFf4646a),
                                                    Color(0xFFec65a4),
                                                  ],
                                                ),
                                                color: const Color(0xFFec65a4),
                                                borderRadius:
                                                    BorderRadius.circular(10),
                                              ),
                                              child: TextButton(
                                                  child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .center,
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .center,
                                                    children: const [
                                                      Text("Submit",
                                                          style: TextStyle(
                                                              fontFamily:
                                                                  'sans-serif',
                                                              fontSize: 18,
                                                              color:
                                                                  Colors.white,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold))
                                                    ],
                                                  ),
                                                  style: ButtonStyle(
                                                      shape: MaterialStateProperty.all<
                                                              RoundedRectangleBorder>(
                                                          RoundedRectangleBorder(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          10.0),
                                                              side: const BorderSide(
                                                                  color: Color(
                                                                      0xFFec65a4))))),
                                                  onPressed: onSubmitPressed),
                                            ),
                                          ],
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            );
                          },
                        ),
                      ));
                } else if (snapshot.hasError) {
                  return Text("${snapshot.error}");
                }
                return const Center(
                  child: Padding(
                      padding: EdgeInsets.all(4.0),
                      child: SizedBox(
                        child: CircularProgressIndicator(),
                        height: 20.0,
                        width: 20.0,
                      )),
                );
              },
            )));
  }
}
